/*
This is the global Files for constants andveriables.
Veriables in this file will be available throughout the app.
@author : Uday G.<udaghulaxe@gmail.com >

*/

//Base url for API
// var serviceBase = 'http://staging.pragmasoftwares.com/webappapi/';
var serviceBase = 'http://localhost/angular/score_api/';
var AUTH0_CLIENT_ID = 'rhDdv9sedkoVvEeHGSvm4iZNRVtL6r1B';
var AUTH0_DOMAIN = 'score.auth0.com';


function handleError(error) {
  return function () {
      return { success: false, message:error };
  };

  //console.log(error);
};