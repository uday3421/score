angular.module('app').factory("DataService", ['$http',function ($http) {
    // This service connects to our REST API
        var obj = {};
        obj.toast = function (data) {
            toaster.pop(data.status, "", data.message, 10000, 'trustedHtml');
        }
        obj.get = function (q) {
            return $http.get(serviceBase + q).then(function (response) {
                return response.data;
            });
        };
        obj.post = function (q, object) {
            return $http.post(serviceBase + q, object).then(function(response) {
                return response.data;
            });
        };
        obj.put = function (q, object) {
            return $http.put(serviceBase + q, object).then(function (response) {
                return response.data;
            });
        };
        obj.delete = function (q) {
            return $http.delete(serviceBase + q).then(function (response) {
                return response.data;
            });
        };

        return obj;
}])

.factory('AuthService',['$rootScope','DataService','auth', '$q','store','$state', function ($rootScope,DataService,auth,$q,store,$state) {

    var authService = {};
    
    authService.logout = function() {
            store.remove('profile');
            store.remove('token');
            auth.signout();
            $state.go('intro');
    };

  return authService;
}])


.factory('profileService',['DataService','$q','store', function (DataService,$q,store) {

    var ProfileService = {};
    
    ProfileService.createInitialProfile = function(profile) {
    var deferred = $q.defer();
    return DataService
      .post('createinitialprofile', profile)
      .then(function(response)
          {
            if(response.state == 'success'){
                deferred.resolve({ success: true, massage: response.massage });
            }
            else if(response.state == 'error'){
                deferred.resolve({ success: false, massage: response.massage});
            }
            return deferred.promise;
          },
          function(response){
            console.log('Error occured');
          });
           
    };

  return ProfileService;
}]);     