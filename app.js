'use strict';

angular.module('app',['auth0','angular-storage','angular-jwt', 'ui.router', 'ng-sortable','ngMaterial'])
.config(['$provide', 'authProvider','$stateProvider','$httpProvider','$urlRouterProvider','$locationProvider','jwtInterceptorProvider', function($provide, authProvider, $stateProvider, $httpProvider, $urlRouterProvider, $locationProvider, jwtInterceptorProvider) {

	   $stateProvider
	    .state('intro', {
	      url: '/intro',
	      templateUrl: 'components/intro/intro.tpl.html',
	    })

	    $stateProvider
	    .state('home', {
	      url: '/home',
	      templateUrl: 'components/home/home.tpl.html',
	      controller: 'homeController as homectrl',
	      data: {
      		requiresLogin: true
    		}
	    })

	    $stateProvider
	    .state('profile', {
	      url: '/profile',
	      templateUrl: 'components/profile/profile.tpl.html',
	      controller: 'profileController as userctrl',
	      data: {
      		requiresLogin: true
    		}
	    });
	    
		// Specify URL states here
	   $urlRouterProvider.otherwise('intro');

	   authProvider.init({
	   	domain : AUTH0_DOMAIN,
	   	clientID : AUTH0_CLIENT_ID,
	   	loginState: 'intro'
	   });

	   
    jwtInterceptorProvider.tokenGetter = function(store) {
      	return store.get('token');
      }
    

    $httpProvider.interceptors.push('jwtInterceptor');

}])





.run(['$rootScope', 'auth', 'store', 'jwtHelper', '$injector', '$location', '$state', function($rootScope, auth, store, jwtHelper, $injector, $location, $state) {
  var refreshingToken = null;


	$rootScope.$on('$stateChangeStart', function(event,next, nextParams, fromState) {
	    var token = store.get('token');
	    var refreshToken = store.get('refreshToken');
	    if (token) {
	      if (!jwtHelper.isTokenExpired(token)) {
	        if (!auth.isAuthenticated) {
	          auth.authenticate(store.get('profile'), token);
	          console.log('here');
	        }
	        if(next.name == 'intro' && fromState.name == 'home'){
				console.log('Dont go back to intro');
				event.preventDefault();
				$state.go($state.current, {}, {reload: true});
			}
	      } 
	    }
	  });


	  $rootScope.$on('$locationChangeStart', function() {
	    var token = store.get('token');
	    var refreshToken = store.get('refreshToken');
	    if (token) {
	      if (!jwtHelper.isTokenExpired(token)) {
	        if (!auth.isAuthenticated) {
	          auth.authenticate(store.get('profile'), token);
	        }
	      } else {
	        if (refreshToken) {
	          if (refreshingToken === null) {
	              refreshingToken =  auth.refreshIdToken(refreshToken).then(function(idToken) {
	                store.set('token', idToken);
	                auth.authenticate(store.get('profile'), idToken);
	              }).finally(function() {
	                  refreshingToken = null;
	              });
	          }
	          return refreshingToken;
	        } else {
	          $location.path('/intro');
	        }
	      }
	    }
	  });
}]);
