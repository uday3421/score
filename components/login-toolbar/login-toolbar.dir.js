(function(){
	angular.module('app')
	.directive('loginToolbar',['auth','store', '$state', function(auth, store, $state){
		return{
			templateUrl : 'components/login-toolbar/login-toolbar.tpl.html',
			controller : 'loginToolbarController',
			controllerAs : 'logintoolbar'
		}
	}])
	.controller('loginToolbarController',['auth','AuthService','profileService','store', '$state','$mdSidenav', function(auth, AuthService,profileService, store, $state, $mdSidenav){
		var vm = this;
		vm.login = login;
		vm.logout = logout;
		vm.auth = auth;

		function login(){
			auth.signin({
				popup: true,
				chrome: true,
				standalone: true
			}, function(profile, token,access_token, state, refresh_token){
				store.set('profile',profile);
				store.set('token',token);
				profileService.createInitialProfile(profile);
				$state.go('home');
			},
			function(error){
				console.log(error);
			})
		}

		function logout(){
			AuthService.logout();
		}
	}]);
})()