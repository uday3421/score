(function(){
	angular.module('app')
	.directive('sidebar',['auth','store', '$location', function(auth, store, $location){
		return{
			templateUrl : 'components/sidebar/sidebar.tpl.html',
			controller : 'sidebarController',
			controllerAs : 'sidebarCtrl'
		}
	}])

	.controller('sidebarController',['auth','store', '$location', function(auth, store, $location){
		var vm = this;
		vm.profile = store.get('profile');
	}]);
})()