(function(){
	angular.module('app')
	.directive('appToolbar',['auth','store', '$location', function(auth, store, $location){
		return{
			templateUrl : 'components/app-toolbar/app-toolbar.tpl.html',
			controller : 'appToolbarController',
			controllerAs : 'apptoolbar'
		}
	}])
	.controller('appToolbarController',['auth','store', 'AuthService', '$location','$mdSidenav', function(auth, store, AuthService, $location, $mdSidenav){

		var vm = this;
		vm.logout = logout;
		vm.toggleSidenav = toggleSidenav;
		vm.profile = store.get('profile');

		function toggleSidenav(menuId) {
	    		$mdSidenav(menuId).toggle();
	  		};

		function logout(){
			AuthService.logout();
		}

	}]);

})()